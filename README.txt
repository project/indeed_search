INTRODUCTION
------------

Indeed Search provides a search block and a page for listing the search result
consisting of jobs listed on Indeed Jobs. It integrates Indeed API with Druapal.

First of all you have to get a publisher ID from indeed.com and enter it the
configuration settings.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


CONFIGURATION
-------------

 * You may want to disable Toolbar module, since its output clashes with
   Administration Menu.

 * Configure values in Configuration » Indeed Search

   - Enter the Indeed Publisher ID.
   - Enter the Query.
   - Enter the Location.
   - Enter the Radius.
   - Enter the Radius.
   - Enter the Limit.
   - Chose the Sort by.
   - Chose the Country.


MAINTAINERS
-----------

Current maintainers:
 * Pritam Prasun (techtud) - https://www.drupal.org/user/838042
 * vidhatanand - https://www.drupal.org/user/585764
 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/user/3495331
